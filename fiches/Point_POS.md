# Point POS :

--

###Qu’est-ce qu’un pos-tagger ?

Le part-of-speech tag, ou "taggage" selon les parties du discours, est un travail de labellisation qui permet de renvoyer tous les mots d’un énoncé à l’ensemble grammatical auquel ils appartiennent. C’est un processus d’étiquetage morpho-syntaxique au niveau du mot qui s’inscrit dans une démarche de linguistique informatique.
Qu’est-ce que ça fait ? Comment ça marche ?

En Traitement Automatique du Langage (TALN/TAL ou NLP), l’étiquetage grammatical ou pos-tagging nous donne avec précision et fiabilité la fonction d’un mot, levant ainsi nombre d’ambiguïtés courantes en français – en particulier lorsqu’elles sont liées au contexte.

Un tag doit donc :

- reconnaître les paramètres grammaticaux du mot selon des classes prédéfinis par le modèle d'étiquetage.

Il existe plusieurs modèles d'étiquetage, ce qui peut poser la question du choix du modèle pertinent par rapport à tel type de texte, par rapport à telle analyse ou problématique de recherche. Par définition, plus le modèle sera compliqué, moins il sera performant. 

Par exemple, les 10 classes morphosyntaxiques (nom commun, nom propre, pronom, adjectif, verbe, préposition, adverbe, conjonction de subordination, conjonction de coordination, interjection) peuvent se voir préciser ou suppléer par quelques catégories plus spécifiques (verbe infinitif, verbe participe passé, pronom complément d’objet).

