# Point lemmatisation :
-- 

### Qu’est-ce qu’un lemmatizer ?

La lemmatisation est un outil de linguistique informatique qui fait un travail d’extraction de la base lexicale des mots. Il ne se fonde pas sur la construction des phrases mais sur le lexique et fait émerger la forme canonique du mot sans flexion (ni les –s du pluriel, ni les –e du féminin par exemple).

### Qu’est-ce que ça fait ? Comment ça marche ?


En Traitement Automatique du Langage Naturel (TALN/TAL ou NLP), la lemmatization rend possible la reconnaissance des mots tels qu’ils se présentent dans le dictionnaire. Elle ôte les marques liées au genre et au nombre, remet tout verbe à l’infinitif.

La lemmatisation permet de retrouver la forme première et générique des mots. Contrairement au stemme qui apparaît sans affixe et laisse le mot méconnaissable, le lemme se contente de le remettre à sa forme générique.

### À quoi ça sert ?

La lemmatisation opère une grande normalisation au sein du texte, en supprimant les variations de mots liées au genre, à la conjugaison et au nombre (pluriel / singulier). Cela est particulièrement utiles pour la fouille d’opinions et la détection d’émotions, car cela permet de faire émerger les grandes tendances sémantiques d’un document.

