# Point tokénisation : 
-- 

### Qu’est-ce qu’un tokenizer ?

Un tokenizer est un outil fondé sur un algorithme basé sur un ensemble de règles ou sur un apprentissage à partir d'un corpus étiqueté manuellement. Il permet de découper le texte en mots. C’est une analyse dite morphologique.
Qu’est-ce que ça fait ? Comment ça marche ?

La tokenisation est un travail de **segmentation** qui décompose une phrase en ses multiples éléments.

En Traitement Automatique des Langues (TAL), la tokenisation fait partie du processus de normalisation. Elle segmente le texte en entrée en unités linguistiques manipulables comme les mots, la ponctuation, les nombres, les données alphanumériques... Chaque élément correspond à un token qui sera utile à l’analyse.

Le but de ce procédé est de séparer les unités de base d’un texte qui se prêteront à une analyse pointue par la suite. On peut penser qu’il suffit de détecter les espaces entre les mots, mais ce n'est pas toujours aussi facile – en particulier pour le français.

Que faire des tirets ? Où couper lorsqu'on rencontre des apostrophes ? Qu’en est-il des locutions nominales comme “fer à repasser” qui sont certes composées de plusieurs mots, mais ne représentent qu’une seule unité de sens ?

Ainsi, si l’on accepte sans problème que le mot arc-en-ciel comporte 3 unités lexicales distinctes (arc/en/ciel), il est plus difficile de déterminer le nombre de mots au sein du groupe "d'accord" ou "d'habitude". Il faut donc bien définir ce qu'est un token avant de poursuivre les traitements.

La reconnaissance de patterns efficaces doit s’adapter aux réalités véhiculées dans les textes présentés à l'analyse... Un tokeniser est complètement dépendant de la langue sur laquelle on travaille.

### UN PEU DE CULTURE GEEK

Le but de la segmentation est de rendre un texte interprétable par une machine. La difficulté est de régulariser un langage naturel qui, par essence, ne l'est pas ! La variabilité, la richesse et l’évolution constante du langage qu’on parle pose autant d’obstacles pour le normaliser. Plusieurs outils sont disponibles pour réaliser l’étape de segmentation qui utilisent l’espace comme premier séparateur. Ce "blanc" est le point de départ de toute tokenisation.

Toutefois, en français, tout n’est pas si simple. Par exemple, le point est souvent utilisé comme marqueur de fin de phrase. Mais il marque également l'utilisation d'une abréviation voire la présence d'une date ou d'un titre (pensons à M., abréviation du nom monsieur). Il faut donc ruser et décider de conserver certains points plutôt que d'autres. Les points finaux et ceux qui marquent des abréviations ne véhiculent pas le même sens.