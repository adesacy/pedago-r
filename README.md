# Pedago-R

## Objectifs : 

L'objectif de ce dépôt est de fournir du matériel pédagogique aux projets, personnels, étudiants en Humanités numériques. L'ensemble de ces matériaux s'appuie sur le langage de programmation R et suit un cycle de vie des projets en traitement des données, allant du nettoyage des données jusqu'à leur mise en valeur par des datavisualisations. Un ensemble de fonctions sont codées de façon à ce qu'elles puissent être utilisées par des projets n'ayant pas forcément une grande connaissance de la programmation. L'ensemble de ces matériaux pédagogiques sont codés dans le langage R.

## Références : 

Nous n'avons, comme souvent, pas codé l'ensemble des fonctions proposées ici et nous sommes appuyés sur différentes sources que nous tâchons de répertorier ici :

- https://juliasilge.com/ ; https://juliasilge.com/blog/
- https://r-graph-gallery.com/index.html
- https://universaldependencies.org/u/pos/
- https://allisonhorst.com/r-packages-functions 
- https://github.com/ufal/udpipe
- https://cran.r-project.org/web/packages/udpipe/vignettes/udpipe-tryitout.html
- https://stackoverflow.com/
- https://ladal.edu.au/coll.html
- Sur les jointures : http://perso.ens-lyon.fr/lise.vaudor/combinaisons-et-jointures-de-tables-avec-dplyr/
- Sur les document term matrix : https://dustinstoltz.com/blog/2020/12/1/creating-document-term-matrix-comparison-in-r
- Et j'en oublie, bien évidemment...

Machine learning and models in R : 


- https://smltar.com/mlregression#mlregressionfull.
- https://www.tidymodels.org/start/recipes/.
- https://simonschoe.github.io/ml-with-tidymodels/
- https://www.tmwr.org/
- tidymodels, parnsip, recipes...

---

Pour les présentations : 
- https://bookdown.org/yihui/rmarkdown/shower-presentations.html.

## Pré-requis :

- Installation préalable de la dernière version de R 4.1.2 et de l'IDE RStudio sur son poste de travail (de préférence Linux ou Mac...). Voir par exemple [https://quanti.hypotheses.org/1813](https://quanti.hypotheses.org/1813).
- Création d'un compte GitLab sur la grille de service Huma-Num pour récupération des documents de travail (données, scripts, md, Rmd...) ou d'un Github.
- Notions de programmation informatique, notamment en R. Voir sur ce point, par exemple, l'excellent BARNIER, Julien, 2022 : [https://juba.github.io/tidyverse/index.html](https://juba.github.io/tidyverse/index.html). 
- Installation préalable des librairies de manipulation de données et d'analyses textuelles : dplyr, tidyverse, tidytext, vroom, ggplot2, stringr, tidyr, UDPipe...

## Proposition de travail :

Sur un corpus textuel et une problématique donnée, les participants apprendront à utiliser le langage R et l'interface de programmation RStudio pour mettre en place un pipeline de traitement textuel. Ces traitements pourront impliquer, suivant le temps dont nous disposerons et les avancées des participants, les opérations suivantes :

* Récupération des données.
* Lecture des données.
* Nettoyage des données (expressions régulières).
* Mise en forme des données (format tidy).
* Annotations (lemmatisation, étiquetage morphosyntaxique, récupération des entités nommées).
* Exploration, statistiques descriptives, visualisations.
* Classification de textes non supervisée à partir des annotations (ACP, CAH, k-means...).
* Topic modelling.
* Stylométrie roulante (Stylo).

## Sujet :

- Y a-t-il une évolution du style de Marcel Proust au fil des sept tomes d'*À la Recherche du Temps perdu ?*

### Autre pistes envisagées : 

- Analyse des reviews de Goodreads : https://www.kaggle.com/datasets/nikhil1e9/goodreads-books
- Analyse des prompts de Chat-GPT : https://www.kaggle.com/datasets/lusfernandotorres/chatpgpt-prompts
- Analyse des streams de Spotify : https://www.kaggle.com/datasets/nelgiriyewithana/top-spotify-songs-2023

### Bibliographie :

- Par exemple dans une optique de dialogue et de réactualisation, avec des méthodes contemporaines, de l'article d'Etienne Brunet : Cf. https://hal.archives-ouvertes.fr/hal-01574516/document. 

- Idem, dialogue, comparaisons, approfondissement de : Cyril Labbé, Dominique Labbé. « Les phrases de Marcel Proust », *14th International Conference on Textual Data Statistical Analysis, Department of Mathematics and Statistics - University of Roma ”la Sapienza”*, June 2018, Roma, Italie. pp.400 - 410.

- Comparaisons : RAGONNEAU, Nicolas, *Le Proustographe : Proust et À la recherche du temps perdu en infographie*, Paris, Denoël, 2021. 

## Séances :

### 1. Installation, familiarisation avec l'IDE et le langage. 

- Installation de R
- Utilisation de Rmarkdown
- Introduction basique sur quelques notions de programmation
	- Manipulations simples d'objets dans R
	- Opérations simples
	- Création d'objets simples
- Création d'un jeu de données
- Création d'un tableau
- Gestion basique des indices
- Utilisation de fonctions basiques
- Gestion de la documentation et des paramètres
- Génération d'un graphique simple sur deux variables
- Exercices
- Exercices bonus

### 2. Récupération et lecture des données, nettoyages, mise en forme et annotations.

- Préparation du corpus et nettoyages.
- Annotation du corpus (lemmatisation, étiquetage morphosyntaxique)
- Tokénisation mots et phrases
- Sauvagearde du corpus annoté

### 3. Exploration, statistiques descriptives, visualisations.

- Exploration du corpus, correction d'erreurs et nettoyages
- Statistiques descriptives (longueur des phrases, fréquence des mots, des POS, ...)
- Pondération des fréquences (tf-idf) et étude des segments distinctifs
- Visualisations.
- Retours aux textes.

### 4. Classification de textes non-supervisée à partir des annotations (ACP, CAH, k-means...).

- Classification ascendante hiérarchique (CAH).
- Analyse en composante principale (ACP).

### 5. Les motifs : 

- https://github.com/Malichot/Motifs/

### 6. Topic modelling.

- Mallet, LDAvis.
- https://medium.com/broadhorizon-cmotions/nlp-with-r-part-1-topic-modeling-to-identify-topics-in-restaurant-reviews-3ee870e6cd8.
- https://rdrr.io/cran/mallet/.
- https://www.youtube.com/watch?v=4YyoMGv1nkc.

### 7. Stylométrie roulante (Stylo).

Les machines à vecteur de support (Support Vector Machines ou S.V.M.) désignent une série d’algorithmes de classification fondés sur l’apprentissage supervisé. Le package stylo combine cela avec une approche du texte séquentielle. L’idée est d’entraîner un algorithme sur un ensemble de textes pour ensuite lui demander une prédiction sur des portions d’un texte dont la paternité, par exemple, est suspecte. À partir de son corpus d’entraînement, l’algorithme va ainsi donner une prédiction sur chacun des segments.

*Références* : 

- Computational Stylistics Group.
- Eder, M. (2016). Rolling stylometry. Digital Scholarship in the Humanities, 31(3): 457–469.
- Eder, M., Rybicki, J. and Kestemont, M. (2016). Stylometry with R: a package for computational text analysis. R Journal, 8(1): 107-21. https://journal.r-project.org/archive/2016/RJ-2016-007/index.html
- R package : stylo : https://github.com/computationalstylistics/stylo
- Utilisation dans TXM : https://groupes.renater.fr/wiki/txm-users/public/tutorial_to_use_stylo_into_txm

### 8. Autres pistes :

#### Réseaux de personnages : 

- Annotation, récupération des entités nommées personnages, création d'un réseau à partir de ces personnages en fonction de leur apparition dans les textes.
- Labex OBVIL avait potentiellement travaillé là-dessus ? 
- Projet du LATTICE : BookNLP : repérage de personnages. 


#### Analyse des bigrams : 
- https://www.tidytextmining.com/ngrams.html

#### Coocurrences
- https://ladal.edu.au/coll.html
- https://medium.com/analytics-vidhya/how-to-create-co-occurrence-networks-with-the-r-packages-cooccur-and-visnetwork-f6e1ceb1c523

#### Extraction de sentiments et d'arcs de tracés basés sur les sentiments à partir de textes : 

- Matthew Jockers.
- https://cran.r-project.org/web/packages/syuzhet/vignettes/syuzhet-vignette.html

#### Analyse de sentiment : 

- https://www.tidytextmining.com/sentiment.html

#### Plongement de mots (word embedding) : 
- Word2vec
- Glove
- Fasttext
- Etc. 

#### Génération automatique de texte : 

- Keras : https://keras.io/examples/nlp/text_generation_fnet
- Tensorflow : https://www.tensorflow.org/text/tutorials/text_generation

### 8. Bibliographie : 

#### Humanités numériques : 

- Cf. EDER, Maciej, *Visualization in stylometry: Cluster analysis using networks, Digital Scholarship in the Humanities*, Volume 32, Issue 1, April 2017, Pages 50–64, https://doi.org/10.1093/llc/fqv061 : https://academic.oup.com/dsh/article/32/1/50/2957386 : *"Even if some issues still remain unresolved, scholars roughly agree that Euclidean (normalized) distance and Ward’s linking algorithm provide acceptable results"*.
- URROWS, John, « Delta : a Measure of Stylistic Difference and a Guide to Likely Au-thorship », *Literary and Linguistic Computing*, 17(3), pp. 267-287.
- Cf. EVERT, S., PROISL, T., JANNIDIS, F., PIELSTROM, S., SCHOCH, C.,VITT, T., « Towards a better understanding of Burrow’s Delta in literary authorship », 2015, p. 6 :« Le point de départ de la représentation du document est un modèle de “sac de mots” du texte, c’est-à-dire que nous comptons combien de fois chaque forme de mot se retrouve dans chaque document. Le nombre de mots est ensuite transformé en fréquences relatives pour compenser les différentes longueurs de texte. Pour le traitement ultérieur, les n mots différents les plus fréquents sur l’ensemble du corpus[...] sont choisis. Dans le modèle d’espace vectoriel, chaque mot différent correspond à une dimension différente. Les fréquences de mots de tous les documents peuvent maintenant être organisées dans une matrice comprenant les mots de chaque corpus. Burrows (2002) “normalise” ensuite les fréquences des mots, c’est-à-dire qu’il normalise les fréquences de telle sorte que, sur l’ensemble du corpus, la moyenne de chaque mot soit égale à 0 et l’écart type à 1 (le résultat est également connu sous le nom de “z-score” [...]. Cela réduit l’influence des mots les plus fréquents. Comme les fréquences des mots suivent la distribution décrite par la loi de Zipf (Zipf 1935), la distance ne serait sinon influencée que par les quelques mots les plus fréquents ». Nous traduisons.
- CAMPS, Jean-Baptiste, CAFIERO, Florian, « Setting bounds in a homoge-neous corpus : A methodological study applied to medieval literature », inRevue des Nouvelles Techno-logies de l’information SHS-1(Modèles et Apprentissages en Sciences Humaines et Sociales Rédacteursinvités), 2013, pp. 55–84.
- CAMPS, Jean-Baptiste, CAFIERO, Florian, « Why Molière most likely did write his plays »,Science Advances, Vol. 5, no. 11, Nov. 2019.
- KESTEMONT, Mike, *Proceedings of the 3rd Workshop on Computational Linguistics for Literature (CLFL)*, Association for Computational Linguistics, 2014, pp. 59–66.
- STRAUSS, T., MALTITZ (von), M. J., « Generalising Ward’s method foruse with manhattan distances »,PLOS ONE 12, 2017 Jan 13 ;12(1) :e0168288.
- MOISL, Hermann, « Finding the Minimum Document Lengthfor Reliable Clustering of Multi-Document Natural Language Corpora », inJournal of QuantitativeLinguistics, February 2011.

#### Statistiques : 

- H. Wickham (2009). ggplot2: Elegant Graphics for Data Analysis. Springer-Verlag New York.
- Maechler, M., Rousseeuw, P., Struyf, A., Hubert, M., Hornik, K.(2016). cluster: Cluster Analysis Basics and Extensions. R package version 2.0.5.
- Sebastien Le, Julie Josse, Francois Husson (2008). FactoMineR: An R Package for Multivariate Analysis. Journal of Statistical Software, 25(1), 1-18. 10.18637/jss.v025.i01
- Tal Galili (2015). *dendextend: an R package for visualizing, adjusting, and comparing trees of hierarchical clustering. Bioinformatics.* DOI: 10.1093/bioinformatics/btv428
- TIBSHIRANI, R., WALTHER, G., HASTLE, T., « Estimating the number of clusters in a data set via the gap statistics », *Journal of the Royal Statistical Society Series B (Statistical Methodology)*, 63, Part. 2, 2001, pp. 411-423.
